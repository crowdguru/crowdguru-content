=== Crowdguru Content ===

Contributors: akindziora
Tags: content generation, crowdsourcing content
Requires at least: 4.7
Tested up to: 5.7.1
Stable tag: 1.0.0
Requires PHP: 5.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
This plugin is a fork of the Basic Authentication handler plugin.

Note that this plugin requires sending your username and password with every
request, and should only be used over SSL-secured connections.

## Installing
1. Download the plugin into your plugins directory
2. Enable in the WordPress admin
3. create a user with role "Author" and assign the E-Mail "texte@crowdguru.de"

## Using
This plugin adds support for Basic Authentication, as specified in [RFC2617][].
Most HTTP clients will allow you to use this authentication natively. 

1. Just create a Post and assign it to the Author previously created. (user E-Mail "texte@crowdguru.de")
2. Write a short Briefing in your Post, how your Content should be written.
3. Mark the Post as a draft
4. Wait for the Text to be written
5. status will change to pending
6. review the content
7. publish the Post

## Simplified workflow
 
![explaining workflow](https://bytebucket.org/crowdguru/crowdguru-content/raw/8b178b62e0041de102c5028090ea04ee47f61fd6/cg-wp-content-plugin.png)
 
[RFC2617]: https://tools.ietf.org/html/rfc2617

## About Us

Order content from Wordpress and deliver it to where it belongs, in your Wordpress. Crowd Guru is a German crowdsourcing service provider and one of the pioneers and market leaders in the German-speaking market. This Wordpress plugin enables the connection to our platform. 

### Contact

it@crowdguru.de

### License

GPLv2 or later

== Changelog ==

1.0.0 latest wording changes 
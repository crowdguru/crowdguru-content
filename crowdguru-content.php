<?php
/**
 * Plugin Name: Crowdguru Content Plugin
 * Plugin URI: https://www.crowdguru.de/leistungen/content-erstellung/
 * Description: Order content from Wordpress and deliver it to where it belongs, in your Wordpress. Crowd Guru is a German crowdsourcing service provider and one of the pioneers and market leaders in the German-speaking market. This Wordpress plugin enables the connection to our platform. 
 * Author: Alexander Kindziora - Crowdguru
 * Author URI: https://www.crowdguru.de/en/about-us/
 * Version: 1.0.0
 * License: GPLv2 or later
 */

function crowdguru_json_basic_auth_handler( $user ) {
	global $crowdguru_json_basic_auth_error_msg;

	$crowdguru_json_basic_auth_error_msg = null;

	// Don't authenticate twice
	if ( ! empty( $user ) ) {
		return $user;
	}

	// Check that we're trying to authenticate
	if ( !isset( $_SERVER['PHP_AUTH_USER'] ) ) {
		return $user;
	}

	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];

	/**
	 * In multi-site, wp_authenticate_spam_check filter is run on authentication. This filter calls
	 * get_currentuserinfo which in turn calls the determine_current_user filter. This leads to infinite
	 * recursion and a stack overflow unless the current function is removed from the determine_current_user
	 * filter during authentication.
	 */
	remove_filter( 'determine_current_user', 'crowdguru_json_basic_auth_handler', 20 );

	$user = wp_authenticate( $username, $password );

	add_filter( 'determine_current_user', 'crowdguru_json_basic_auth_handler', 20 );

	if ( is_wp_error( $user ) ) {
		$crowdguru_json_basic_auth_error_msg = $user;
		return null;
	}

	$crowdguru_json_basic_auth_error_msg = true;

	return $user->ID;
}
add_filter( 'determine_current_user', 'crowdguru_json_basic_auth_handler', 20 );

function crowdguru_json_basic_auth_error( $error ) {
	// Passthrough other errors
	if ( ! empty( $error ) ) {
		return $error;
	}

	global $crowdguru_json_basic_auth_error_msg;

	return $crowdguru_json_basic_auth_error_msg;
}
add_filter( 'rest_authentication_errors', 'crowdguru_json_basic_auth_error' );
